<?php

namespace app\assets;

use yii\web\AssetBundle;

class NotifyAsset extends AssetBundle {
    public $sourcePath = '@bower';
    public $js = [
        'remarkable-bootstrap-notify/dist/bootstrap-notify.js'
    ];
}
