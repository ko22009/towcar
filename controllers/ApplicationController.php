<?php

namespace app\controllers;

use app\models\ApplicationHistory;
use app\models\ModelCustomerCar;
use Yii;
use app\models\Application;
use app\models\ApplicationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ApplicationController implements the CRUD actions for Application model.
 */
class ApplicationController extends Controller {

    public function actionFirmChange($id) {
        $model = new Application();
        return $model->getModel_by_firm($id);
    }

    /**
     * Lists all Application models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ApplicationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 5;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Application model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Application model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Application();

        $model_history = new ApplicationHistory();
        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            $post['ApplicationHistory']['application_id'] = $model->id;
            if($model_history->load($post) && $model_history->save())
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'model_history' => $model_history
        ]);
    }

    /**
     * Updates an existing Application model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $mode_customer_car = ModelCustomerCar::find()->where(['id' => $model->model_customer_car_id])->all();
        //$model->model_customer_car_id - узнаем firm_id
        $model->firm_id = $mode_customer_car;

        $model_history = new ApplicationHistory();
        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            $post['ApplicationHistory']['application_id'] = $model->id;
            if($model_history->load($post) && $model_history->save())
            return $this->redirect(['view', 'id' => $model->id]);
        }
        $last = $model->getLast_application_history();
        $model_history->stage_id = $last->stage_id;
        $model_history->arrival_place = $last->arrival_place;
        $model_history->departure_place = $last->departure_place;

        $model_history->stage_id = $last->stage_id;
        $model_history->arrival_place = $last->arrival_place;
        $model_history->departure_place = $last->departure_place;
        $model_history->evacuation_type_id = $last->evacuation_type_id;
        $model_history->evacuation_car_id = $last->evacuation_car_id;
        $model_history->dispatching_id = $last->dispatching_id;

        $model_history->driver_id = $last->driver_id;
        $model_history->driver_price = $last->driver_price;
        $model_history->date_start = $last->date_start;
        $model_history->date_start_type_id = $last->date_start_type_id;
        $model_history->date_finish = $last->date_finish;

        return $this->render('update', [
            'model' => $model,
            'model_history' => $model_history
        ]);
    }

    /**
     * Finds the Application model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Application the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Application::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
