<?php

namespace app\controllers;

use app\models\Stage;
use yii\data\ArrayDataProvider;
use yii\web\Controller;

/**
 * StageController implements the CRUD actions for Stage model.
 */
class StageController extends Controller {

    /**
     * Lists all Stage models.
     * @return mixed
     */
    public function actionIndex() {
        $model = new Stage();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $model->getList(),
            'modelClass' => $model,
            'pagination' => [
                'pageSize' => 11,
            ],
        ]);
        return $this->render('index', [
            'data' => $dataProvider,
        ]);
    }
}
