<?php

namespace app\controllers;

use app\models\WhatIsIt;
use Yii;
use app\models\WhatIsItSearch;
use yii\data\ArrayDataProvider;
use yii\web\Controller;

/**
 * WhatIsItController implements the CRUD actions for WhatIsIt model.
 */
class WhatIsItController extends Controller {

    /**
     * Lists all WhatIsIt models.
     * @return mixed
     */
    public function actionIndex() {
        $model = new WhatIsIt();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $model->getList(),
            'modelClass' => $model,
            'pagination' => [
                'pageSize' => 11,
            ],
        ]);

        return $this->render('index', [
            'data' => $dataProvider,
        ]);
    }
}
