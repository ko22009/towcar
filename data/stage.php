<?php

use app\models\Stage;

return [
    [Stage::NEW_APPL, 'Не обработана заявка'],
    [Stage::ACCEPT_APPL, 'Принятая заявка'],
    [Stage::IN_QUEUE_APPL, 'В очереди'],
    [Stage::HOLD_APPL, 'Отложенная заявка'],
    [Stage::NOT_ACCEPT_APPL, 'Не принятая заявка'],
    [Stage::TRANSMIT_APPL, 'Переданная заявка'],
    [Stage::POTENTION_APPL, 'Потенциальная заявка'],
    [Stage::NOT_TARGET_APPL, 'Не целевой звонок'],
    [Stage::FINISH_APPL, 'Заявка выполнена'],
    [Stage::FAKE_APPL, 'Ложный вызов'],
    [Stage::CLOSED_APPL, 'Закрытая заявка']
];
