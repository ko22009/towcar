<?php

use app\models\WhatIsIt;

return [
    [WhatIsIt::NOT_ENGINE, 'Не заводится'],
    [WhatIsIt::NOT_HARD,'Без сложностей'],
    [WhatIsIt::CRASH,'Аварийный'],
    [WhatIsIt::BLOCK_WHEEL,'Заблокировано одно колесо и более'],
    [WhatIsIt::NOT_NEUTRAL,'Не ставится нейтраль'],
    [WhatIsIt::IN_FLY,'Висит на предмете'],
    [WhatIsIt::NO_STEERING_WHEEL,'Не крутится руль'],
    [WhatIsIt::CLOSED_AUTO, 'Автомобиль закрыт'],
    [WhatIsIt::DITCH,'Кювет'],
];