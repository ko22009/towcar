const gulp = require('gulp');
const browserify = require('browserify');
const babelify = require('babelify');
const moduleImporter = require('sass-module-importer');
const stripCssComments = require('gulp-strip-css-comments');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const rename = require('gulp-rename');
const scss = require('gulp-sass');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const browserSync	= require('browser-sync').create();
const cleanCSS = require('gulp-clean-css');

const mainJSDir = './web/js/';
const mainJS = 'entry.js';
const buildJSPath = 'web';

const scssDir = './web/css/**/*.scss';
const buildSCSSPath = 'web';

console.log(process.cwd());

gulp.task('scss-dev', function() {
	return gulp.src(scssDir)
		.pipe(sourcemaps.init())
		.pipe(scss({ outputStyle: 'expanded', importer: moduleImporter()}).on('error', scss.logError))
		.pipe(rename({suffix: '.min'}))
		.pipe(sourcemaps.write('./'))
		.pipe(cleanCSS())
		.pipe(gulp.dest(buildSCSSPath))
		.pipe(browserSync.reload({stream: true}));
});

gulp.task('scss-prod', function() {
	return gulp.src(scssDir)
		.pipe(scss({ outputStyle: 'compressed', importer: moduleImporter() }).on('error', scss.logError))
		.pipe(rename({suffix: '.min'}))
		.pipe(stripCssComments())
		.pipe(cleanCSS())
		.pipe(gulp.dest(buildSCSSPath));
});

function bundleJS(bundler) {
	return bundler.bundle()
		.on('error', function(err){
			// print the error (can replace with gulp-util)
			console.log(err.message);
			// end this stream
			this.emit('end');
		})
		.pipe(source(mainJS))
		.pipe(buffer())
		.pipe(rename({ suffix: '.min' }))
		.pipe(sourcemaps.init({ loadMaps: true }))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(buildJSPath))
		.pipe(browserSync.reload({stream: true}));
}

gulp.task('js-dev', function() {
	const bundler = browserify(mainJSDir + mainJS, { debug: true }).transform(babelify);
	bundleJS(bundler);
});

gulp.task('js-prod', function() {
	const bundler = browserify(mainJSDir + mainJS).transform(babelify);
	bundler.bundle()
		.pipe(source(mainJS))
		.pipe(buffer())
		.pipe(rename({ suffix: '.min' }))
		.pipe(uglify())
		.pipe(gulp.dest(buildJSPath));
});

gulp.task('browser-sync', function() {
	browserSync.init({
		proxy: 'towcar.dev',
		ui: {
			port: 8081,
			weinre: {
				port: 8083
			}
		},
		port: 8000,
		notify: false,
		open: false // Stop the browser from automatically opening
	});
});

gulp.task('dev-watch', ['js-dev', 'browser-sync', 'scss-dev'], function() {
	gulp.watch('web/js/**/*.js', ['js-dev']);
	gulp.watch('web/css/**/*.css', ['scss-dev']);
	gulp.watch('web/css/**/*.scss', ['scss-dev']);
});

gulp.task('default', ['dev-watch']);