<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `application`.
 */
class m171127_161510_create_application_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('application', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull(),
            'what_is_it' => $this->string(),
            'state_number_num' => $this->integer(),
            'state_number_lit' => $this->string(),
            'state_number_reg' => $this->integer(),
            'state_number_country_id' => $this->integer(),
            'model_customer_car_id' => $this->integer()->notNull(),
            'weight_car' => $this->float(),
            'payment_type_id' => $this->integer()->notNull(),
            'source_traffic_id' => $this->integer(),
            'feedback' => $this->text(),
            'closed' => $this->boolean()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('application');
    }
}
