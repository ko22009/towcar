<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer`.
 */
class m171127_175005_create_customer_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('customer', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'phone' => $this->string(),
            'email' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('customer');
    }
}
