<?php

use yii\db\Migration;

/**
 * Handles the creation of table `evacuation_type`.
 */
class m171127_181034_create_evacuation_type_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('evacuation_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('evacuation_type');
    }
}
