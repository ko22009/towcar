<?php

use yii\db\Migration;

/**
 * Handles the creation of table `model_customer_car`.
 */
class m171127_181701_create_model_customer_car_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('model_customer_car', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'firm_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('model_customer_car');
    }
}
