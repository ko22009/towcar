<?php

use yii\db\Migration;

/**
 * Handles the creation of table `firm_customer_car`.
 */
class m171127_184947_create_firm_customer_car_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('firm_customer_car', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('firm_customer_car');
    }
}
