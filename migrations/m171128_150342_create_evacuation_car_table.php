<?php

use yii\db\Migration;

/**
 * Handles the creation of table `evacuation_car`.
 */
class m171128_150342_create_evacuation_car_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('evacuation_car', [
            'id' => $this->primaryKey(),
            'state_number' => $this->string(),
            'name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('evacuation_car');
    }
}
