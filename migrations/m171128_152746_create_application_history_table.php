<?php

use yii\db\Migration;

/**
 * Handles the creation of table `application_history`.
 */
class m171128_152746_create_application_history_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('application_history', [
            'id' => $this->primaryKey(),
            'application_id' => $this->integer()->notNull(),
            'date' => $this->dateTime(),
            'stage_id' => $this->integer()->notNull(),
            'date_start' => $this->dateTime(),
            'date_start_type_id' => $this->integer(),
            'date_finish' => $this->dateTime(),
            'driver_id' => $this->integer(),
            'departure_place' => $this->text(),
            'arrival_place' => $this->text(),
            'evacuation_type_id' => $this->integer(),
            'evacuation_car_id' => $this->integer(),
            'order_price' => $this->money(11, 2),
            'driver_price' => $this->money(11, 2),
            'dispatching_id' => $this->integer(),
            'dispatching_price' => $this->money(11, 2),
            'distance' => $this->float(),
            'comment' => $this->text()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('application_history');
    }
}
