<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tree_path_cost`.
 */
class m171130_125058_create_tree_path_cost_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('tree_path_cost', [
            'id' => $this->primaryKey(),
            'ancestor' => $this->integer()->notNull(),
            'descendant' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('tree_path_cost');
    }
}
