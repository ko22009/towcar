<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cost_type`.
 */
class m171130_125320_create_cost_type_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('cost_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'subject_cost_type_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('cost_type');
    }
}
