<?php

use yii\db\Migration;

/**
 * Handles the creation of table `worker`.
 */
class m171130_130343_create_worker_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('worker', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'third_name' => $this->string(),
            'position_id' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('worker');
    }
}
