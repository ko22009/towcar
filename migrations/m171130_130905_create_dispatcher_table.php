<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dispatcher`.
 */
class m171130_130905_create_dispatcher_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('dispatcher', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('dispatcher');
    }
}
