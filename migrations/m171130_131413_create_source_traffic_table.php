<?php

use yii\db\Migration;

/**
 * Handles the creation of table `source_traffic`.
 */
class m171130_131413_create_source_traffic_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('source_traffic', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('source_traffic');
    }
}
