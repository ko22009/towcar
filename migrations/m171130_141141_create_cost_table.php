<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cost`.
 */
class m171130_141141_create_cost_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('cost', [
            'id' => $this->primaryKey(),
            'subject_cost_type_id' => $this->integer()->notNull(),
            'subject_id' => $this->integer()->notNull(),
            'cost' => $this->money(11, 2),
            'cost_type_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('cost');
    }
}
