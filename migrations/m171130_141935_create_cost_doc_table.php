<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cost_doc`.
 */
class m171130_141935_create_cost_doc_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('cost_doc', [
            'id' => $this->primaryKey(),
            'doc_type_id' => $this->integer()->notNull(),
            'link' => $this->text()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('cost_doc');
    }
}
