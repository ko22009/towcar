<?php

use yii\db\Migration;

/**
 * Handles the creation of table `doc_type`.
 */
class m171130_142327_create_doc_type_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('doc_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('doc_type');
    }
}
