<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subject_cost_type`.
 */
class m171130_142813_create_subject_cost_type_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('subject_cost_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('subject_cost_type');
    }
}
