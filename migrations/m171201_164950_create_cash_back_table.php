<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cash_back`.
 */
class m171201_164950_create_cash_back_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('cash_back', [
            'id' => $this->primaryKey(),
            'worker_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('cash_back');
    }
}
