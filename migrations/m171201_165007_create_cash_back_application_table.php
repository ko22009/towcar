<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cash_back_application`.
 */
class m171201_165007_create_cash_back_application_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('cash_back_application', [
            'id' => $this->primaryKey(),
            'application_id' => $this->integer()->notNull(),
            'cash_back_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('cash_back_application');
    }
}
