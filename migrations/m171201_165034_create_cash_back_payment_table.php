<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cash_back_payment`.
 */
class m171201_165034_create_cash_back_payment_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('cash_back_payment', [
            'id' => $this->primaryKey(),
            'cash_back_id' => $this->integer()->notNull(),
            'money' => $this->money(11, 2),
            'date' => $this->dateTime(),
            'payment_type_id' => $this->integer()->notNull(),
            'comment' => $this->text()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('cash_back_payment');
    }
}
