<?php

use yii\db\Migration;

/**
 * Handles the creation of table `what_is_it`.
 */
class m171201_195947_create_what_is_it_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('what_is_it', [
            'id' => $this->primaryKey(),
            'name' => $this->text()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('what_is_it');
    }
}
