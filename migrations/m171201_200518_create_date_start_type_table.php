<?php

use yii\db\Migration;

/**
 * Handles the creation of table `date_start_type`.
 */
class m171201_200518_create_date_start_type_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('date_start_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('date_start_type');
    }
}
