<?php

use yii\db\Migration;

/**
 * Class m171204_122240_create_foreign_key_position_to_worker
 */
class m171204_122240_create_foreign_key_position_to_worker extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addForeignKey(
            'fk-worker-position_id',
            'worker',
            'position_id',
            'position',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropForeignKey(
            'fk-worker-position_id',
            'worker'
        );
    }
}
