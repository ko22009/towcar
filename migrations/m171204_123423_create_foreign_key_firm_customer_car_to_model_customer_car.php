<?php

use yii\db\Migration;

/**
 * Class m171204_123423_create_foreign_key_firm_customer_car_to_model_customer_car
 */
class m171204_123423_create_foreign_key_firm_customer_car_to_model_customer_car extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addForeignKey(
            'fk-model_customer_car-firm_id',
            'model_customer_car',
            'firm_id',
            'firm_customer_car',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropForeignKey(
            'fk-model_customer_car-firm_id',
            'model_customer_car'
        );
    }
}
