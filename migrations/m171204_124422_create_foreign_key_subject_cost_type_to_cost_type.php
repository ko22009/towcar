<?php

use yii\db\Migration;

/**
 * Class m171204_124422_create_foreign_key_subject_cost_type_to_cost_type
 */
class m171204_124422_create_foreign_key_subject_cost_type_to_cost_type extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addForeignKey(
            'fk-cost_type-subject_cost_type_id',
            'cost_type',
            'subject_cost_type_id',
            'subject_cost_type',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropForeignKey(
            'fk-cost_type-subject_cost_type_id',
            'cost_type'
        );
    }
}
