<?php

use yii\db\Migration;

/**
 * Class m171204_125111_create_foreign_key_to_cost
 */
class m171204_125111_create_foreign_key_to_cost extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addForeignKey(
            'fk-cost-subject_cost_type_id',
            'cost',
            'subject_cost_type_id',
            'subject_cost_type',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-cost-cost_type_id',
            'cost',
            'cost_type_id',
            'cost_type',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropForeignKey(
            'fk-cost-subject_cost_type_id',
            'cost'
        );
        $this->dropForeignKey(
            'fk-cost-cost_type_id',
            'cost'
        );
    }
}
