<?php

use yii\db\Migration;

/**
 * Class m171204_130209_create_foreign_key_doc_type_id_to_cost_doc
 */
class m171204_130209_create_foreign_key_doc_type_id_to_cost_doc extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addForeignKey(
            'fk-cost_doc-doc_type_id',
            'cost_doc',
            'doc_type_id',
            'doc_type',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropForeignKey(
            'fk-cost_doc-doc_type_id',
            'cost_doc'
        );
    }
}
