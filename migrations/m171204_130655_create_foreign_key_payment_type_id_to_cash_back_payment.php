<?php

use yii\db\Migration;

/**
 * Class m171204_130655_create_foreign_key_payment_type_id_to_cash_back_payment
 */
class m171204_130655_create_foreign_key_payment_type_id_to_cash_back_payment extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addForeignKey(
            'fk-cash_back_payment-payment_type_id',
            'cash_back_payment',
            'payment_type_id',
            'payment_type',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropForeignKey(
            'fk-cash_back_payment-payment_type_id',
            'cash_back_payment'
        );
    }
}
