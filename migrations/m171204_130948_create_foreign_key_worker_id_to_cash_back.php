<?php

use yii\db\Migration;

/**
 * Class m171204_130948_create_foreign_key_worker_id_to_cash_back
 */
class m171204_130948_create_foreign_key_worker_id_to_cash_back extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addForeignKey(
            'fk-cash_back-worker_id',
            'cash_back',
            'worker_id',
            'worker',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropForeignKey(
            'fk-cash_back-worker_id',
            'cash_back'
        );
    }
}
