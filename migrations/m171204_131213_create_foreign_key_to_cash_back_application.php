<?php

use yii\db\Migration;

/**
 * Class m171204_131213_create_foreign_key_to_cash_back_application
 */
class m171204_131213_create_foreign_key_to_cash_back_application extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addForeignKey(
            'fk-cash_back_application-application_id',
            'cash_back_application',
            'application_id',
            'application',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-cash_back_application-cash_back_id',
            'cash_back_application',
            'cash_back_id',
            'cash_back',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropForeignKey(
            'fk-cash_back_application-application_id',
            'cash_back_application'
        );
        $this->dropForeignKey(
            'fk-cash_back_application-cash_back_id',
            'cash_back_application'
        );
    }
}
