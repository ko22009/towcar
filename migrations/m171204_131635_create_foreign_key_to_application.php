<?php

use yii\db\Migration;

/**
 * Class m171204_131635_create_foreign_key_to_application
 */
class m171204_131635_create_foreign_key_to_application extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addForeignKey(
            'fk-application_customer_id',
            'application',
            'customer_id',
            'customer',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-application_model_customer_car_id',
            'application',
            'model_customer_car_id',
            'model_customer_car',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-application_payment_type_id',
            'application',
            'payment_type_id',
            'payment_type',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-application_source_traffic_id',
            'application',
            'source_traffic_id',
            'source_traffic',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropForeignKey(
            'fk-application_customer_id',
            'application'
        );
        $this->dropForeignKey(
            'fk-application_model_customer_car_id',
            'application'
        );
        $this->dropForeignKey(
            'fk-application_payment_type_id',
            'application'
        );
        $this->dropForeignKey(
            'fk-application_source_traffic_id',
            'application'
        );
    }
}
