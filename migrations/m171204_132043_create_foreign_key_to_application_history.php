<?php

use yii\db\Migration;

/**
 * Class m171204_132043_create_foreign_key_to_application_history
 */
class m171204_132043_create_foreign_key_to_application_history extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->addForeignKey(
            'fk-application_history-application_id',
            'application_history',
            'application_id',
            'application',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-application_history-date_start_type_id',
            'application_history',
            'date_start_type_id',
            'date_start_type',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-application_history-evacuation_type_id',
            'application_history',
            'evacuation_type_id',
            'evacuation_type',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-application_history-evacuation_car_id',
            'application_history',
            'evacuation_car_id',
            'evacuation_car',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-application_history-dispatching_id',
            'application_history',
            'dispatching_id',
            'dispatcher',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropForeignKey(
            'fk-application_history-application_id',
            'application_history'
        );

        $this->dropForeignKey(
            'fk-application_history-date_start_type_id',
            'application_history'
        );

        $this->dropForeignKey(
            'fk-application_history-evacuation_type_id',
            'application_history'
        );

        $this->dropForeignKey(
            'fk-application_history-evacuation_car_id',
            'application_history'
        );

        $this->dropForeignKey(
            'fk-application_history-dispatching_id',
            'application_history'
        );
    }
}
