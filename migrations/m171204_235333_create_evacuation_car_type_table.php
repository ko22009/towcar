<?php

use yii\db\Migration;

/**
 * Handles the creation of table `evacuation_car_type`.
 */
class m171204_235333_create_evacuation_car_type_table extends Migration {
    /**
     * @inheritdoc
     */
    public function safeUp() {
        $this->createTable('evacuation_car_type', [
            'id' => $this->primaryKey(),
            'evacuation_car_id' => $this->integer()->notNull(),
            'evacuation_type_id' => $this->integer()->notNull()
        ]);
        $this->addForeignKey(
            'fk-evacuation_car_type-evacuation_car_id',
            'evacuation_car_type',
            'evacuation_car_id',
            'evacuation_car',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-evacuation_car_type-evacuation_type_id',
            'evacuation_car_type',
            'evacuation_type_id',
            'evacuation_car',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropForeignKey(
            'fk-evacuation_car_type-evacuation_car_id',
            'evacuation_car_type'
        );
        $this->dropForeignKey(
            'fk-evacuation_car_type-evacuation_type_id',
            'evacuation_car_type'
        );
        $this->dropTable('evacuation_car_type');
    }
}
