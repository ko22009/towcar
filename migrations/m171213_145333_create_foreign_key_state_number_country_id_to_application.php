<?php

use yii\db\Migration;

/**
 * Class m171213_145333_create_foreign_key_state_number_country_id_to_application
 */
class m171213_145333_create_foreign_key_state_number_country_id_to_application extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-application-state_number_country_id',
            'application',
            'state_number_country_id',
            'country',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-application-state_number_country_id',
            'application'
        );
    }
}
