<?php

use yii\db\Migration;

/**
 * Handles the creation of table `region`.
 */
class m171213_173044_create_region_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('region', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'number' => $this->integer()
        ]);
        $this->addForeignKey(
            'fk-region-country_id',
            'region',
            'country_id',
            'country',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-region-country_id',
            'region'
        );
        $this->dropTable('region');
    }
}
