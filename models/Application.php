<?php

namespace app\models;

use Yii;
use app\models\Stage;

/**
 * This is the model class for table "application".
 *
 * @property int $id
 * @property int $customer_id
 * @property string $what_is_it
 * @property int $state_number_num
 * @property string $state_number_lit
 * @property int $state_number_reg
 * @property int $state_number_country_id
 * @property int $model_customer_car_id
 * @property double $weight_car
 * @property int $payment_type_id
 * @property int $source_traffic_id
 * @property string $feedback
 * @property mixed $customer
 * @property mixed $customer_name
 * @property mixed $model
 * @property void $last_application_history
 * @property string $model_name_with_firm
 * @property mixed $what_is_it_names
 * @property int $closed
 */
class Application extends \yii\db\ActiveRecord {

    public $firm_id;
    public $stage_id;
    public $phone;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'application';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['customer_id', 'model_customer_car_id', 'payment_type_id', 'firm_id'], 'required'],
            [['customer_id', 'model_customer_car_id', 'payment_type_id', 'source_traffic_id', 'closed', 'firm_id'], 'integer'],
            [['feedback'], 'string'],
            [['weight_car'], 'number'],
            [['what_is_it', 'state_number_num', 'state_number_lit', 'state_number_reg', 'state_number_country_id', 'phone'], 'safe'],
        ];
    }

    // преобразуем в json
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->what_is_it)
                $this->what_is_it = json_encode($this->what_is_it);
            return true;
        } else {
            return false;
        }
    }

    public function getModel_by_firm($firm) {
        $records = ModelCustomerCar::find()->with('firm_customer_car')->where(['firm_id' => $firm])->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return json_encode((object)$output);
    }

    public function getWhat_is_it_names() {
        $arr = str_replace(['"', "[", ']'], '', $this->what_is_it);
        $arr = explode(",", $arr);
        $records = WhatIsIt::getName($arr);
        return implode(',', $records);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'customer_id' => 'Клиент',
            'what_is_it' => 'Что с машиной',
            'state_number_num' => 'Гос. номер - цифры',
            'state_number_lit' => 'Гос. номер - буквы',
            'state_number_reg' => 'Гос. номер - регион',
            'state_number_country_id' => 'Гос. номер - страна',
            'model_customer_car_id' => 'Модель авто клиента',
            'weight_car' => 'Вес машины (кг)',
            'payment_type_id' => 'Тип оплаты',
            'source_traffic_id' => 'Источник трафика',
            'feedback' => 'Обратная связь клиента',
            'closed' => 'Закрыта заявка?',
            'phone' => 'Телефон'
        ];
    }

    public static function getCustomer_list() {
        $records = Customer::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
            if ($record['phone'])
                $output[$record['id']] .= ' | ' . $record['phone'];
        }
        return $output;
    }

    public function getCustomer() {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    public function getCustomer_name() {
        return isset($this->customer->name)?$this->customer->name:'';
    }

    public function getModel() {
        return $this->hasOne(ModelCustomerCar::className(), ['id' => 'model_customer_car_id']);
    }

    public function getModel_name_with_firm() {
        return $this->model->firm_name . ' - ' . $this->model->name;
    }

    public static function getWhat_is_it_list() {
        $records = WhatIsIt::getList();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

    public static function getFirm_list() {
        $records = FirmCustomerCar::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

    public static function getModel_customer_car_list_with_firm() {
        $records = ModelCustomerCar::find()->with('firm_customer_car')->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['firm_customer_car']['name'] . ' - ' . $record['name'];
        }
        return $output;
    }

    public static function getModel_customer_car_list() {
        $records = ModelCustomerCar::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

    public static function getPayment_type_list() {
        $records = PaymentType::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

    public static function getSource_traffic_list() {
        $records = SourceTraffic::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

    public function getLast_history() {
        return $this->hasMany(ApplicationHistory::className(), ['application_id' => 'id']);
    }

    public function getApplication_history() {
        return $this->hasMany(ApplicationHistory::className(), ['application_id' => 'id'])->orderBy(['id'=>SORT_DESC])->one();
    }

    public function getLast_application_history() {
        return $this->application_history;
    }

    public function getStage_name($id) {
        return Stage::getStage($id);
    }

    public function getCountry_list() {
        $records = Country::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }
    public function getRegion_list() {
        return Region::getRegion_list('');
    }

    public function getPhone() {
        return $this->customer->phone;
    }

    public static function getPhone_list() {
        $records = Customer::find()->all();
        $output = [];
        foreach ($records as $record) {
            if ($record['phone'])
                $output[$record['id']] = $record['phone'];
        }
        return $output;
    }
}
