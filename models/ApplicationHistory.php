<?php

namespace app\models;

use yii\db\Expression;

/**
 * This is the model class for table "application_history".
 *
 * @property int $id
 * @property int $application_id
 * @property string $date
 * @property int $stage_id
 * @property string $date_start
 * @property int $date_start_type_id
 * @property string $date_finish
 * @property int $driver_id
 * @property string $departure_place
 * @property string $arrival_place
 * @property int $evacuation_type_id
 * @property int $evacuation_car_id
 * @property string $order_price
 * @property string $driver_price
 * @property int $dispatching_id
 * @property string $dispatching_price
 * @property double $distance
 * @property string $comment
 */
class ApplicationHistory extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'application_history';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['application_id', 'stage_id'], 'required'],
            [['application_id', 'stage_id', 'date_start_type_id', 'driver_id', 'evacuation_type_id', 'evacuation_car_id', 'dispatching_id'], 'integer'],
            [['date', 'date_start', 'date_finish'], 'safe'],
            [['departure_place', 'arrival_place', 'comment'], 'string'],
            [['order_price', 'driver_price', 'dispatching_price', 'distance'], 'number'],
            ['date', 'default',
                'value' => new Expression('NOW()')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'application_id' => 'Application ID',
            'date' => 'Дата создания',
            'stage_id' => 'Этап',
            'date_start' => 'Дата начала',
            'date_start_type_id' => 'Тип даты начала',
            'date_finish' => 'Дата окончания',
            'driver_id' => 'Водитель',
            'departure_place' => 'Место отправления',
            'arrival_place' => 'Место назначения',
            'evacuation_type_id' => 'Тип эвакуатора',
            'evacuation_car_id' => 'Эвакуатор',
            'order_price' => 'Стоимость заказа',
            'driver_price' => 'Тариф',
            'dispatching_id' => 'Диспетчерские',
            'dispatching_price' => 'Диспетчерские деньги',
            'distance' => 'Дистанция',
            'comment' => 'Комментарий',
        ];
    }

    public function getEvacuation_car_by_type($type) {
        $records = EvacuationCarType::find()->with('evacuation_car')->where(['evacuation_type_id' => $type])->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['evacuation_car']['id']] = $record['evacuation_car']['name'];
        }
        return json_encode((object)$output);
    }

    public static function getApplication_list() {
        $records = Application::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['id'];
        }
        return $output;
    }

    public function getApplication() {
        return $this->hasOne(Application::className(), ['id' => 'application_id']);
    }

    public function getApplication_name() {
        return $this->application->id;
    }

    public static function getStage_list() {
        $records = Stage::getList();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

    public function getStage() {
        return self::getStage_list()[$this->stage_id];
    }

    public static function getDriver_list() {
        $records = Worker::find()->where(['position_id' => Worker::DRIVER])->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

    public static function getDate_start_type_list() {
        $records = DateStartType::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

    public static function getEvacuation_type_list() {
        $records = EvacuationType::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

    public static function getEvacuation_car_list($evacuation_type_id) {
        if($evacuation_type_id) $records = EvacuationCar::find()->where(['id' => $evacuation_type_id])->all();
        else $records = EvacuationCar::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

    public static function getDispatching_list() {
        $records = Dispatcher::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

}
