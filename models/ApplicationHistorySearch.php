<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApplicationHistory;

/**
 * ApplicationHistorySearch represents the model behind the search form of `app\models\ApplicationHistory`.
 */
class ApplicationHistorySearch extends ApplicationHistory {
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'application_id', 'stage_id', 'date_start_type_id', 'driver_id', 'evacuation_type_id', 'evacuation_car_id', 'dispatching_id'], 'integer'],
            [['date', 'date_start', 'date_finish', 'departure_place', 'arrival_place', 'comment'], 'safe'],
            [['order_price', 'driver_price', 'dispatching_price', 'distance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ApplicationHistory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'application_id' => $this->application_id,
            'date' => $this->date,
            'stage_id' => $this->stage_id,
            'date_start' => $this->date_start,
            'date_start_type_id' => $this->date_start_type_id,
            'date_finish' => $this->date_finish,
            'driver_id' => $this->driver_id,
            'evacuation_type_id' => $this->evacuation_type_id,
            'evacuation_car_id' => $this->evacuation_car_id,
            'order_price' => $this->order_price,
            'driver_price' => $this->driver_price,
            'dispatching_id' => $this->dispatching_id,
            'dispatching_price' => $this->dispatching_price,
            'distance' => $this->distance,
        ]);

        $query->andFilterWhere(['like', 'departure_place', $this->departure_place])
            ->andFilterWhere(['like', 'arrival_place', $this->arrival_place])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
