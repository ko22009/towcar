<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Application;
use yii\db\Query;

/**
 * ApplicationSearch represents the model behind the search form of `app\models\Application`.
 */
class ApplicationSearch extends Application {
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'customer_id', 'model_customer_car_id', 'payment_type_id', 'source_traffic_id', 'closed'], 'integer'],
            [['what_is_it', 'state_number', 'feedback', 'stage_id', 'phone'], 'safe'],
            [['weight_car'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {

        $subQuery = (new Query())->select(['max(id) _id'])->from(['application_history'])->groupBy('application_id');
        $subQuery = (new Query())->select(['*'])->from(['application_history'])->rightJoin(['history' => $subQuery], 'history._id = application_history.id');

        $subQuery2 = (new Query())->select(['*'])->from(['customer']);

        $query = Application::find()->leftJoin(['application_history' => $subQuery], 'application_history.application_id = application.id')->leftJoin(['customer' => $subQuery2], 'customer.id = application.customer_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'model_customer_car_id' => $this->model_customer_car_id,
            'weight_car' => $this->weight_car,
            'payment_type_id' => $this->payment_type_id,
            'source_traffic_id' => $this->source_traffic_id,
            'closed' => $this->closed,
            'stage_id' => $this->stage_id,
            'customer.id' => $this->phone
        ]);

        $query->andFilterWhere(['like', 'what_is_it', $this->what_is_it])
            ->andFilterWhere(['like', 'feedback', $this->feedback]);

        return $dataProvider;
    }
}
