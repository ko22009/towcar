<?php

namespace app\models;

use Yii;
use app\models\Worker;

/**
 * This is the model class for table "cash_back".
 *
 * @property int $id
 * @property int $worker_id
 */
class CashBack extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'cash_back';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['worker_id'], 'required'],
            [['worker_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'worker_id' => 'Worker ID',
        ];
    }

    public function getWorker() {
        return $this->hasOne(Worker::className(), ['id' => 'worker_id']);
    }

    public function getWorker_name() {
        return $this->worker->name;
    }

    public static function getWorker_list() {
        $records = Worker::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }
}
