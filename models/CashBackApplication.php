<?php

namespace app\models;

use Yii;
use app\models\Application;
use app\models\CashBack;

/**
 * This is the model class for table "cash_back_application".
 *
 * @property int $id
 * @property int $application_id
 * @property int $cash_back_id
 */
class CashBackApplication extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'cash_back_application';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['application_id', 'cash_back_id'], 'required'],
            [['application_id', 'cash_back_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'application_id' => 'Application ID',
            'cash_back_id' => 'Cash Back ID',
        ];
    }

    public static function getApplication_list() {
        $records = Application::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['id'];
        }
        return $output;
    }

    public static function getCashBack_list() {
        $records = CashBack::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['id'];
        }
        return $output;
    }
}
