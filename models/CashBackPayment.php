<?php

namespace app\models;

use Yii;
use app\models\PaymentType;

/**
 * This is the model class for table "cash_back_payment".
 *
 * @property int $id
 * @property string $money
 * @property string $date
 * @property int $payment_type_id
 * @property string $comment
 */
class CashBackPayment extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'cash_back_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['money'], 'number'],
            [['date'], 'safe'],
            [['payment_type_id'], 'required'],
            [['payment_type_id'], 'integer'],
            [['comment'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'money' => 'Money',
            'date' => 'Date',
            'payment_type_id' => 'Payment Type ID',
            'comment' => 'Comment',
        ];
    }

    public function getPayment_type() {
        return $this->hasOne(PaymentType::className(), ['id' => 'payment_type_id']);
    }

    public function getPayment_type_name() {
        return $this->payment_type->name;
    }

    public static function getPayment_type_list() {
        $records = PaymentType::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }
}
