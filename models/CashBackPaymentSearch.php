<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CashBackPaymentSearch represents the model behind the search form of `app\models\CashBackPayment`.
 */
class CashBackPaymentSearch extends CashBackPayment {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'payment_type_id'], 'integer'],
            [['money'], 'number'],
            [['date', 'comment'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = CashBackPayment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'firm_id' => [
                'asc' => ['payment_type.name' => SORT_ASC],
                'desc' => ['payment_type.name' => SORT_DESC],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['payment_type']);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'money' => $this->money,
            'payment_type_id' => $this->payment_type_id,
        ]);

        if (isset($this->date) && $this->date != '') {
            $date_explode = explode(" - ", $this->date);
            $date1 = trim($date_explode[0]);
            $date2 = trim($date_explode[1]);
            $query->andFilterWhere(['between', 'date', $date1, $date2]);
        }

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
