<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CashBack;

/**
 * CashBackSearch represents the model behind the search form of `app\models\CashBack`.
 */
class CashBackSearch extends CashBack {
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'worker_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = CashBack::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'firm_id' => [
                'asc' => ['worker.name' => SORT_ASC],
                'desc' => ['worker.name' => SORT_DESC],
            ]
        ]);

        $dataProvider->setSort($sort);

        $this->load($params);

        $query->joinWith(['worker']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'worker_id' => $this->worker_id,
        ]);

        return $dataProvider;
    }
}
