<?php

namespace app\models;

use Yii;
use app\models\SubjectCostType;
use app\models\CostType;

/**
 * This is the model class for table "cost".
 *
 * @property int $id
 * @property int $subject_cost_type_id
 * @property int $subject_id
 * @property string $cost
 * @property int $cost_type_id
 */
class Cost extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'cost';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['subject_cost_type_id', 'subject_id', 'cost_type_id'], 'required'],
            [['subject_cost_type_id', 'subject_id', 'cost_type_id'], 'integer'],
            [['cost'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'subject_cost_type_id' => 'Subject Cost Type ID',
            'subject_id' => 'Subject ID',
            'cost' => 'Cost',
            'cost_type_id' => 'Cost Type ID',
        ];
    }

    public static function getSubject_cost_type_list() {
        $records = SubjectCostType::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

    public function getSubject_cost_type() {
        return $this->hasOne(SubjectCostType::className(), ['id' => 'subject_cost_type_id']);
    }

    public function getSubject_cost_type_name() {
        return $this->subject_cost_type->name;
    }

    public static function getCost_type_list() {
        $records = CostType::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

    public function getCost_type() {
        return $this->hasOne(CostType::className(), ['id' => 'cost_type_id']);
    }

    public function getCost_type_name() {
        return $this->cost_type->name;
    }
}
