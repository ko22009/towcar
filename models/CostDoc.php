<?php

namespace app\models;

use Yii;
use app\models\DocType;

/**
 * This is the model class for table "cost_doc".
 *
 * @property int $id
 * @property int $doc_type_id
 * @property string $link
 */
class CostDoc extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'cost_doc';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['doc_type_id'], 'required'],
            [['doc_type_id'], 'integer'],
            [['link'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'doc_type_id' => 'Doc Type ID',
            'link' => 'Link',
        ];
    }

    public function getDoc_type() {
        return $this->hasOne(DocType::className(), ['id' => 'doc_type_id']);
    }

    public function getDoc_type_name() {
        return $this->doc_type->name;
    }

    public static function getDoc_type_list() {
        $records = DocType::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }
}
