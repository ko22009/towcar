<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CostDoc;

/**
 * CostDocSearch represents the model behind the search form of `app\models\CostDoc`.
 */
class CostDocSearch extends CostDoc {
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'doc_type_id'], 'integer'],
            [['link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = CostDoc::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'firm_id' => [
                'asc' => ['doc_type.name' => SORT_ASC],
                'desc' => ['doc_type.name' => SORT_DESC],
            ]
        ]);

        $dataProvider->setSort($sort);

        $this->load($params);

        $query->joinWith(['doc_type']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'doc_type_id' => $this->doc_type_id,
        ]);

        $query->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }
}
