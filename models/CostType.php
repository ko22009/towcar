<?php

namespace app\models;

use Yii;
use app\models\SubjectCostType;

/**
 * This is the model class for table "cost_type".
 *
 * @property int $id
 * @property string $name
 * @property int $subject_cost_type_id
 */
class CostType extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'cost_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['subject_cost_type_id'], 'required'],
            [['subject_cost_type_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'subject_cost_type_id' => 'Subject Cost Type ID',
        ];
    }

    public function getSubject_cost_type() {
        return $this->hasOne(SubjectCostType::className(), ['id' => 'subject_cost_type_id']);
    }

    public function getSubject_cost_name() {
        return $this->subject_cost_type->name;
    }

    public static function getSubject_cost_type_list() {
        $records = SubjectCostType::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }
}
