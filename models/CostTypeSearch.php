<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CostType;

/**
 * CostTypeSearch represents the model behind the search form of `app\models\CostType`.
 */
class CostTypeSearch extends CostType {
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'subject_cost_type_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = CostType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'firm_id' => [
                'asc' => ['subject_cost_type.name' => SORT_ASC],
                'desc' => ['subject_cost_type.name' => SORT_DESC],
            ]
        ]);

        $dataProvider->setSort($sort);

        $this->load($params);

        $query->joinWith(['subject_cost_type']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'subject_cost_type_id' => $this->subject_cost_type_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
