<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "date_start_type".
 *
 * @property int $id
 * @property string $name
 */
class DateStartType extends \yii\db\ActiveRecord {
    const EXACTLY_TIME = 1;
    const EXACTLY_TIME_VALUE = 'Точное время';
    const RELATIVE_TIME = 2;
    const RELATIVE_TIME_VALUE = 'Приблизительное время';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'date_start_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
