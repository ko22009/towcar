<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "evacuation_car".
 *
 * @property int $id
 * @property string $state_number
 * @property string $name
 */
class EvacuationCar extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'evacuation_car';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['state_number', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'state_number' => 'State Number',
            'name' => 'Name',
        ];
    }
}
