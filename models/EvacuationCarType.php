<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "evacuation_car_type".
 *
 * @property int $id
 * @property int $evacuation_car_id
 * @property int $evacuation_type_id
 */
class EvacuationCarType extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'evacuation_car_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['evacuation_car_id', 'evacuation_type_id'], 'required'],
            [['evacuation_car_id', 'evacuation_type_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'evacuation_car_id' => 'Evacuation Car ID',
            'evacuation_type_id' => 'Evacuation Type ID',
        ];
    }

    public static function getEvacuation_car_list() {
        $records = EvacuationCar::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

    public static function getEvacuation_type_list() {
        $records = EvacuationType::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }

    public function getEvacuation_car() {
        return $this->hasOne(EvacuationCar::className(), ['id' => 'evacuation_car_id']);
    }

    public function getEvacuation_car_name() {
        return $this->evacuation_car->name;
    }

    public function getEvacuation_type() {
        return $this->hasOne(EvacuationType::className(), ['id' => 'evacuation_type_id']);
    }

    public function getEvacuation_type_name() {
        return $this->evacuation_type->name;
    }
}
