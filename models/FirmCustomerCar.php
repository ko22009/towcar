<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "firm_customer_car".
 *
 * @property int $id
 * @property string $name
 */
class FirmCustomerCar extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'firm_customer_car';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
