<?php

namespace app\models;

use Yii;
use app\models\FirmCustomerCar;

/**
 * This is the model class for table "model_customer_car".
 *
 * @property int $id
 * @property string $name
 * @property int $firm_id
 */
class ModelCustomerCar extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'model_customer_car';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['firm_id'], 'required'],
            [['firm_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'firm_id' => 'Firm ID',
        ];
    }

    public function getFirm_customer_car() {
        return $this->hasOne(FirmCustomerCar::className(), ['id' => 'firm_id']);
    }

    public function getFirm_name() {
        return $this->firm_customer_car->name;
    }

    public static function getFirm_customer_list() {
        $records = FirmCustomerCar::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }
}
