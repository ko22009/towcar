<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ModelCustomerCar;

/**
 * ModelCustomerCarSearch represents the model behind the search form of `app\models\ModelCustomerCar`.
 */
class ModelCustomerCarSearch extends ModelCustomerCar {
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'firm_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = ModelCustomerCar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $sort = $dataProvider->getSort();
        $sort->attributes = array_merge($sort->attributes, [
            'firm_id' => [
                'asc' => ['firm_customer_car.name' => SORT_ASC],
                'desc' => ['firm_customer_car.name' => SORT_DESC],
            ]
        ]);

        $dataProvider->setSort($sort);

        $this->load($params);

        $query->joinWith(['firm_customer_car']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'firm_id' => $this->firm_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
