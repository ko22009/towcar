<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "region".
 *
 * @property int $id
 * @property int $country_id
 * @property string $name
 * @property int $number
 */
class Region extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id'], 'required'],
            [['country_id', 'number'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'name' => 'Name',
            'number' => 'Number',
        ];
    }

    public static function getRegion_list($country = '') {
        $output = [];
        if($country) {
            $records = Region::find()->where(['country_id' => $country])->all();
        } else {
            $records = [];
        }
        foreach ($records as $record) {
            $output[$record['id']] = $record['number'] . ' | ' . $record['name'];
        }
        return json_encode((object)$output);
    }

    public static function getCountry_list($country = '') {
        $output = [];
        if($country) {
            $records = Country::find()->where(['id' => $country])->all();
        } else {
            $records = Country::find()->all();
        }
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }
}
