<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "source_traffic".
 *
 * @property int $id
 * @property string $name
 */
class SourceTraffic extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'source_traffic';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
