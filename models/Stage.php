<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class "stage".
 *
 * @property int $id
 * @property string $name
 */
class Stage extends Model {
    public $id;
    public $name;

    const NEW_APPL = 1;
    const ACCEPT_APPL = 2;
    const IN_QUEUE_APPL = 3;
    const HOLD_APPL = 4;
    const NOT_ACCEPT_APPL = 5;
    const TRANSMIT_APPL = 6;
    const POTENTION_APPL = 7;
    const NOT_TARGET_APPL = 8;
    const FINISH_APPL = 9;
    const FAKE_APPL = 10;
    const CLOSED_APPL = 11;

    private static function _getList($_data) {
        $data = [];
        for ($i = 0; $i < count($_data); $i++) {
            $data[$_data[$i][0]] = ['id' => $_data[$i][0], 'name' => $_data[$i][1]];
        }
        return $data;
    }

    public static function getList() {
        $file = include(Yii::$app->basePath . '/data/stage.php');
        return self::_getList($file);
    }

    public static function getStage($id) {
        $list = self::getList();
        if (isset($list[$id]['name']))
        return $list[$id]['name'];
        else return '';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }
}
