<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subject_cost_type".
 *
 * @property int $id
 * @property string $name
 */
class SubjectCostType extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'subject_cost_type';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
