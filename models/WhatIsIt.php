<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "what_is_it".
 *
 * @property int $id
 * @property string $name
 */
class WhatIsIt extends Model {

    const NOT_ENGINE = 1;
    const NOT_HARD = 2;
    const CRASH = 3;
    const BLOCK_WHEEL = 4;
    const NOT_NEUTRAL = 5;
    const IN_FLY = 6;
    const NO_STEERING_WHEEL = 7;
    const CLOSED_AUTO = 8;
    const DITCH = 9;
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'what_is_it';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    private static function _getList($_data) {
        $data = [];
        for ($i = 0; $i < count($_data); $i++) {
            $data[$_data[$i][0]] = ['id' => $_data[$i][0], 'name' => $_data[$i][1]];
        }
        return $data;
    }

    public static function getList() {
        $file = include(Yii::$app->basePath . '/data/whatisit.php');
        return self::_getList($file);
    }

    public static function getName($arr) {
        $list = self::getList();
        $output = [];
        foreach ($arr as $key => $value)
        {
            if(isset($list[$value]['name']))
            $output[] = $list[$value]['name'];
        }
        return $output;
    }

}
