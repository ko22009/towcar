<?php

namespace app\models;

/**
 * This is the model class for table "worker".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $third_name
 * @property int $position_id
 */
class Worker extends \yii\db\ActiveRecord {
    const DRIVER = 2;
    const DRIVER_VALUE = 'Водитель';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'worker';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['position_id'], 'integer'],
            [['first_name', 'last_name', 'third_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'third_name' => 'Отчество',
            'position_id' => 'Должность',
        ];
    }

    public function getPosition() {
        return $this->hasOne(Position::className(), ['id' => 'position_id']);
    }

    public function getPositionName() {
        return $this->position->name;
    }

    public function getName() {
        return $this->last_name . ' ' . $this->first_name . ' ' . $this->third_name;
    }

    public static function getPositionList() {
        $records = Position::find()->all();
        $output = [];
        foreach ($records as $record) {
            $output[$record['id']] = $record['name'];
        }
        return $output;
    }
}
