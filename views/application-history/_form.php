<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ApplicationHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="application-history-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'application_id')->widget(Select2::className(), [
        'data' => $model->getApplication_list(),
        'options' => [
            'placeholder' => 'Please select one',
            'allowClear' => true
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'date')->textInput(['readonly' => 'true']) ?>

    <?= $form->field($model, 'stage_id')->widget(Select2::className(), [
        'data' => $model->getStage_list(),
        'options' => [
            'placeholder' => '',
            'allowClear' => true
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'date_start')->widget(DateTimePicker::className(), [
        'name' => 'date_start',
        'options' => ['placeholder' => 'Выберите дату и время'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd hh:ii:00',
            'todayHighlight' => true
        ]
    ]) ?>

    <?= $form->field($model, 'date_start_type_id')->widget(Select2::className(), [
        'data' => $model->getDate_start_type_list(),
        'options' => [
            'placeholder' => '',
            'allowClear' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'date_finish')->widget(DateTimePicker::className(), [
        'name' => 'date_finish',
        'options' => ['placeholder' => 'Выберите дату и время'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd hh:ii:00',
            'todayHighlight' => true
        ]
    ]) ?>

    <?= $form->field($model, 'driver_id')->widget(Select2::className(), [
        'data' => $model->getDriver_list(),
        'options' => [
            'placeholder' => '',
            'allowClear' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'departure_place')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'arrival_place')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'evacuation_type_id')->widget(Select2::className(), [
        'data' => $model->getEvacuation_type_list(),
        'options' => [
            'placeholder' => '',
            'allowClear' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>
    <?= $form->field($model, 'evacuation_car_id')->widget(Select2::className(), [
        'data' => $model->getEvacuation_car_list($model->evacuation_type_id),
        'options' => [
            'placeholder' => '',
            'allowClear' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'order_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'driver_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dispatching_id')->widget(Select2::className(), [
        'data' => $model->getDispatching_list(),
        'options' => [
            'placeholder' => '',
            'allowClear' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'dispatching_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'distance')->textInput() ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
