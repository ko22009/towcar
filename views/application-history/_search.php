<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplicationHistorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="application-history-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'application_id') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'stage_id') ?>

    <?= $form->field($model, 'date_start') ?>

    <?php // echo $form->field($model, 'date_start_type_id') ?>

    <?php // echo $form->field($model, 'date_finish') ?>

    <?php // echo $form->field($model, 'driver_id') ?>

    <?php // echo $form->field($model, 'departure_place') ?>

    <?php // echo $form->field($model, 'arrival_place') ?>

    <?php // echo $form->field($model, 'evacuation_type_id') ?>

    <?php // echo $form->field($model, 'evacuation_car_id') ?>

    <?php // echo $form->field($model, 'order_price') ?>

    <?php // echo $form->field($model, 'driver_price') ?>

    <?php // echo $form->field($model, 'dispatching_id') ?>

    <?php // echo $form->field($model, 'dispatching_price') ?>

    <?php // echo $form->field($model, 'distance') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
