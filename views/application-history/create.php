<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApplicationHistory */

$this->title = 'Create Application History';
$this->params['breadcrumbs'][] = ['label' => 'Application Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
