<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

use app\models\ApplicationHistory;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplicationHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Application Histories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-history-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a('Create Application History', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'application_id',
                'filter' => ApplicationHistory::getApplication_list(),
                'value' => function($model) {
                    /* @var app\models\ApplicationHistory $model */
                    return $model->getApplication_name();
                }
            ],
            'date',
            [
                'attribute' => 'stage_id',
                'filter' => ApplicationHistory::getStage_list(),
                'value' => function($model) {
                    /* @var app\models\ApplicationHistory $model */
                    return $model->getStage();
                }
            ],
            'date_start',
            //'date_start_type_id',
            //'date_finish',
            //'driver_id',
            //'departure_place:ntext',
            //'arrival_place:ntext',
            //'evacuation_type_id',
            //'evacuation_car_id',
            //'order_price',
            //'driver_price',
            //'dispatching_id',
            //'dispatching_price',
            //'distance',
            //'comment:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}'
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
