<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplicationHistory */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Application Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-history-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'application_id',
            'date',
            'stage_id',
            'date_start',
            'date_start_type_id',
            'date_finish',
            'driver_id',
            'departure_place:ntext',
            'arrival_place:ntext',
            'evacuation_type_id',
            'evacuation_car_id',
            'order_price',
            'driver_price',
            'dispatching_id',
            'dispatching_price',
            'distance',
            'comment:ntext',
        ],
    ]) ?>

</div>
