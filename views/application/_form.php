<?php

use app\models\Country;
use app\models\Region;
use kartik\checkbox\CheckboxX;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\widgets\MySelect2;
use app\widgets\MyInput;
use app\widgets\ModalWidget;
use app\models\Customer;

/* @var $this yii\web\View */
/* @var $model app\models\Application */
/* @var $form yii\widgets\ActiveForm */
/* @var $model_history app\models\ApplicationHistory */
?>

<?= ModalWidget::widget(['view' => '/customer/_form', 'model' => Customer::className(), 'id' => 'customer_modal', 'title' => 'Создание клиента', 'button' => 0]) ?>
<?= ModalWidget::widget(['view' => '/country/_form', 'model' => Country::className(), 'id' => 'country_modal', 'title' => 'Создание страны', 'button' => 0]) ?>
<?= ModalWidget::widget(['view' => '/region/_form', 'model' => Region::className(), 'id' => 'region_modal', 'title' => 'Создание региона', 'button' => 0]) ?>

<div class="application-form">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'clearfix']]); ?>
    <?= $form->field($model, 'customer_id')->widget(MySelect2::className(), [
        'list' => $model->getCustomer_list(),
        'modal_id' => '#customer_modal'
    ]); ?>
    <? $model->what_is_it = json_decode($model->what_is_it); ?>
    <?= $form->field($model, 'what_is_it')->widget(Select2::className(), [
        'name' => 'what_is_it',
        'data' => $model->getWhat_is_it_list(),
        'options' => [
            'placeholder' => '',
            'allowClear' => true,
            'multiple' => true
        ],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [',', ' '],
            'allowClear' => true
        ],
    ]); ?>

    <div class="row">
    <?= MyInput::widget(['model' => $model, 'attribute' => 'state_number_num', 'type' => 'number']);?>
    <?= MyInput::widget(['model' => $model, 'attribute' => 'state_number_lit']);?>
    <?= MyInput::widget(['model' => $model, 'attribute' => 'state_number_reg', 'type' => 'select2', 'modal_id' => '#region_modal', 'list' => []]);?>
    <?= MyInput::widget(['model' => $model, 'attribute' => 'state_number_country_id', 'type' => 'select2', 'modal_id' => '#country_modal', 'list' => $model->getCountry_list()]);?>

    </div>

    <?= $form->field($model, 'firm_id')->widget(Select2::className(), [
        'data' => $model->getFirm_list(),
        'options' => [
            'placeholder' => '',
            'allowClear' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Марка авто клиента'); ?>

    <?= $form->field($model, 'model_customer_car_id')->widget(Select2::className(), [
        'data' => $model->getModel_customer_car_list(),
        'options' => [
            'placeholder' => '',
            'allowClear' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'weight_car')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'payment_type_id')->dropDownList(
        $model->getPayment_type_list(),
        [
            'prompt' => '',
            'options' => ['1' => ['selected' => true]]
        ]
    ); ?>

    <?= $form->field($model, 'source_traffic_id')->dropDownList(
        $model->getSource_traffic_list(),
        ['prompt' => '']
    ); ?>

    <?= MyInput::widget([
        'list' => $model_history->getStage_list(),
        'model' => $model_history,
        'attribute' => 'stage_id',
        'type' => 'select2',
        'col_size' => ''
    ]); ?>

    <?= $form->field($model_history, 'departure_place')->textInput(); ?>
    <?= $form->field($model_history, 'arrival_place')->textInput(); ?>

    <?= $form->field($model_history, 'evacuation_type_id')->widget(Select2::className(), [
        'data' => $model_history->getEvacuation_type_list(),
        'options' => [
            'placeholder' => '',
            'allowClear' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model_history, 'evacuation_car_id')->widget(Select2::className(), [
        'data' => $model_history->getEvacuation_car_list($model_history->evacuation_type_id),
        'options' => [
            'placeholder' => '',
            'allowClear' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model_history, 'driver_id')->widget(Select2::className(), [
        'data' => $model_history->getDriver_list(),
        'options' => [
            'placeholder' => '',
            'allowClear' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model_history, 'date_start')->widget(DateTimePicker::className(), [
        'name' => 'date_start',
        'options' => ['placeholder' => 'Выберите дату и время'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd hh:ii:00',
            'todayHighlight' => true
        ]
    ]) ?>

    <?= $form->field($model_history, 'date_start_type_id')->widget(Select2::className(), [
        'data' => $model_history->getDate_start_type_list(),
        'options' => [
            'placeholder' => '',
            'allowClear' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model_history, 'date_finish')->widget(DateTimePicker::className(), [
        'name' => 'date_finish',
        'options' => ['placeholder' => 'Выберите дату и время'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd hh:ii:00',
            'todayHighlight' => true
        ]
    ]) ?>

    <?= $form->field($model_history, 'order_price')->textInput(['type' => 'number']); ?>
    <?= $form->field($model_history, 'driver_price')->textInput(['type' => 'number']); ?>

    <?= $form->field($model_history, 'dispatching_id')->widget(Select2::className(), [
        'data' => $model_history->getDispatching_list(),
        'options' => [
            'placeholder' => '',
            'allowClear' => true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= $form->field($model_history, 'dispatching_price')->textInput(['type' => 'number']); ?>

    <?= $form->field($model_history, 'distance')->textInput(['type' => 'number']); ?>
    <?= $form->field($model_history, 'comment')->textInput(); ?>

    <?= $form->field($model, 'feedback')->textarea(['rows' => 6]) ?>

    <?php //= $form->field($model, 'closed')->widget(CheckboxX::className(), ['pluginOptions'=>['threeState'=>false]]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
