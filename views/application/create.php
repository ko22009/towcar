<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Application */
/* @var $model_history app\models\ApplicationHistory */

$this->title = 'Создание заявки';
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_history'=> $model_history,
    ]) ?>

</div>
