<?php

use app\models\ApplicationHistory;
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use app\models\Application;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(['enablePushState' => false]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать заявку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'customer_id',
                'filterInputOptions' => [
                    'id' => 'application-customer_id'
                ],
                'filter' => Application::getCustomer_list(),
                'value' => function($model) {
                    /* @var app\models\Application $model */
                    if (isset($model->customer))
                        return $model->getCustomer_name();
                    else return '';
                }
            ],
            [
                'attribute' => 'what_is_it',
                'filter' => Application::getWhat_is_it_list(),
                'value' => function($model) {
                    /* @var app\models\Application $model */
                    return $model->getWhat_is_it_names();
                }
            ],
            [
                'label' => 'Статус заявки',
                'attribute' => 'stage_id',
                'filter' => ApplicationHistory::getStage_list(),
                'value' => function($model) {
                    /* @var app\models\Application $model */
                    $id = $model->last_application_history;
                    if(isset($id->stage_id))
                    {
                        $stage = $model->getStage_name($id->stage_id);
                        return $stage;
                    } else return '';
                }
            ],
            [
                'label' => 'Гос. номер',
                'format' => 'html',
                'value' => function($model) {
                    /* @var app\models\Application $model */
                    return 'Номер: ' . $model->state_number_num . '<br>' .
                    'Буквы: ' . $model->state_number_lit . '<br>' .
                    'Регион: ' . $model->state_number_reg . '<br>' .
                    'Страна: ' . $model->state_number_country_id;
                }
            ],
            [
                'attribute' => 'model_customer_car_id',
                'filter' => Application::getModel_customer_car_list_with_firm(),
                'value' => function($model) {
                    /* @var app\models\Application $model */
                    return $model->getModel_name_with_firm();
                }
            ],
            [
                'attribute' => 'phone',
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => [
                        'placeholder' => '',
                        'allowClear' => true,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ],
                'filter' => Application::getPhone_list(),
                'value' => function($model) {
                    /* @var app\models\Application $model */
                    return $model->getPhone();
                }
            ],
            //'weight_car',
            //'payment_type_id',
            //'source_traffic_id',
            //'feedback:ntext',
            //'closed',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}'
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
