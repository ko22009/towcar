<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Application */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'customer_id',
                'value' => function($model) {
                    /* @var app\models\Application $model */
                    return $model->getCustomer_name();
                }
            ],
            [
                'attribute' => 'what_is_it',
                'value' => function($model) {
                    /* @var app\models\Application $model */
                    return $model->getWhat_is_it_names();
                }
            ],
            'state_number_num',
            'state_number_lit',
            'state_number_reg',
            'state_number_country_id',
            'model_customer_car_id',
            'weight_car',
            'payment_type_id',
            'source_traffic_id',
            'feedback:ntext',
            'closed',
        ],
    ]) ?>

</div>
