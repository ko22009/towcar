<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CashBackApplication */

$this->title = 'Create Cash Back Application';
$this->params['breadcrumbs'][] = ['label' => 'Cash Back Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-back-application-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
