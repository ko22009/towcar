<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use app\models\CashBackApplication;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CashBackApplicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cash Back Applications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-back-application-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cash Back Application', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'application_id',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'application_id',
                    'data' => CashBackApplication::getApplication_list(),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'hideSearch' => true,
                    'options' => [
                        'placeholder' => 'Выберите №',
                    ]
                ]),
            ],
            [
                'attribute' => 'cash_back_id',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'cash_back_id',
                    'data' => CashBackApplication::getCashBack_list(),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'hideSearch' => true,
                    'options' => [
                        'placeholder' => 'Выберите №',
                    ]
                ]),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
