<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CashBackPayment */

$this->title = 'Create Cash Back Payment';
$this->params['breadcrumbs'][] = ['label' => 'Cash Back Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-back-payment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
