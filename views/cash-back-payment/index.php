<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\CashBackPayment;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CashBackPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cash Back Payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-back-payment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Create Cash Back Payment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'money',
            [
                'attribute' => 'date',
                'format' => 'date',
                'filterType' => \kartik\grid\GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => ([
                    'attribute' => 'only_date',
                    'presetDropdown' => true,
                    'convertFormat' => false,
                    'pluginOptions' => [
                        'separator' => ' - ',
                        'format' => 'YYYY-MM-DD',
                        'locale' => [
                            'format' => 'YYYY-MM-DD'
                        ],
                    ],
                    'pluginEvents' => [
                        "apply.daterangepicker" => "function() { apply_filter('only_date') }",
                    ]
                ])
            ],
            [
                'attribute' => 'payment_type_id',
                'filter' => CashBackPayment::getPayment_type_list(),
                'value' => function($model) {
                    /* @var app\models\CashBackPayment $model */
                    return $model->getPayment_type_name();
                }
            ],
            'comment:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'beforeGrid' => 'My fancy content before.',
            'afterGrid' => 'My fancy content after.',
        ]
    ]); ?>
</div>
