<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CashBackPayment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cash Back Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-back-payment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'money',
            'date:datetime',
            [
                'attribute' => 'payment_type_id',
                'value' => function($model) {
                    /* @var app\models\CashBackPayment $model */
                    return $model->getPayment_type_name();
                }
            ],
            'comment:ntext',
        ],
    ]) ?>

</div>
