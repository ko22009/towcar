<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CashBack */

$this->title = 'Create Cash Back';
$this->params['breadcrumbs'][] = ['label' => 'Cash Backs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-back-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
