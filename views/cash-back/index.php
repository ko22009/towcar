<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\CashBack;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CashBackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cash Backs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-back-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cash Back', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'worker_id',
                'filter' => CashBack::getWorker_list(),
                'value' => function($model) {
                    /* @var app\models\CashBack $model */
                    return $model->getWorker_name();
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
