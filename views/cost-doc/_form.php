<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CostDoc */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cost-doc-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'doc_type_id')->dropDownList(
        $model->getDoc_type_list(),
        ['prompt' => '']
    ); ?>

    <?= $form->field($model, 'link')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
