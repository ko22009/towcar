<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CostDoc */

$this->title = 'Create Cost Doc';
$this->params['breadcrumbs'][] = ['label' => 'Cost Docs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cost-doc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
