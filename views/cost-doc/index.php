<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\CostDoc;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CostDocSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cost Docs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cost-doc-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cost Doc', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'doc_type_id',
                'filter' => CostDoc::getDoc_type_list(),
                'value' => function($model) {
                    /* @var app\models\CostDoc $model */
                    return $model->getDoc_type_name();
                }
            ],
            'link:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
