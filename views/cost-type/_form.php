<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CostType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cost-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subject_cost_type_id')->dropDownList(
        $model->getSubject_cost_type_list(),           // Flat array ('id'=>'label')
        ['prompt' => '']    // options
    ); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
