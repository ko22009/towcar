<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use app\models\Cost;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Costs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cost-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a('Create Cost', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'subject_cost_type_id',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'subject_cost_type_id',
                    'data' => Cost::getSubject_cost_type_list(),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'hideSearch' => true,
                    'options' => [
                        'placeholder' => 'Выберите №',
                    ]
                ]),
            ],
            'subject_id',
            'cost',
            [
                'attribute' => 'cost_type_id',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'cost_type_id',
                    'data' => Cost::getCost_type_list(),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'hideSearch' => true,
                    'options' => [
                        'placeholder' => 'Выберите №',
                    ]
                ]),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'responsive' => true,
        'hover' => true
    ]); ?>
    <?php Pjax::end(); ?>
</div>
