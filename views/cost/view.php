<?php

use yii\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cost */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Costs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cost-view">

    <?= DetailView::widget([
        'model' => $model,
        'condensed' => true,
        'hover' => true,
        'mode' => DetailView::MODE_VIEW,
        'panel' => [
            'heading' => 'Book # ' . $model->id,
            'type' => DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'id',
            [
                'attribute' => 'subject_cost_type_id',
                'type' => DetailView::INPUT_SELECT2,
                'value' => $model->getSubject_cost_type_name(),
                'widgetOptions' => [
                    'data' => $model->getSubject_cost_type_list()
                ],
            ],
            'subject_id',
            'cost',
            [
                'attribute' => 'cost_type_id',
                'type' => DetailView::INPUT_SELECT2,
                'value' => $model->getCost_type_name(),
                'widgetOptions' => [
                    'data' => $model->getCost_type_list()
                ],
            ],
        ]
    ]); ?>

</div>
