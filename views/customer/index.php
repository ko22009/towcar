<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
// pjax подгружает часть страницы заново, из-за этого название страницы меняется
$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
// заменяет на просто Customer, восстанавливаем, как в layout/main
?>
<div class="customer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <? // по-умолчанию отправляет get запросом и перекидывает на страницу - enablePushState. Фильтры без enablePushState пишутся в get параметры и страница перезагружается?>
    <?php Pjax::begin(['enablePushState' => false, 'timeout' => 10000]); ?>
    <p>
        <?= Html::a('Create Customer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <? // yii\grid\ActionColumn по-умолчанию data-pjax = 0?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'phone',
            'email:email',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Действия',
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'delete' => function($url, $model, $id) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                            'title' => 'Удалить',
                            'data-id' => $id,
                            'data-pjax' => 0,
                            'class' => 'customer-delete'
                        ]);
                    }

                ],
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
