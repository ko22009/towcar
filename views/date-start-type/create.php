<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DateStartType */

$this->title = 'Create Date Start Type';
$this->params['breadcrumbs'][] = ['label' => 'Date Start Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="date-start-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
