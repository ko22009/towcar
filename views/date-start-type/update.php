<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DateStartType */

$this->title = 'Update Date Start Type: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Date Start Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="date-start-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
