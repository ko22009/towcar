<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Dispatcher */

$this->title = 'Create Dispatcher';
$this->params['breadcrumbs'][] = ['label' => 'Dispatchers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dispatcher-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
