<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\EvacuationCarType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="evacuation-car-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'evacuation_car_id')->widget(Select2::className(), [
        'data' => $model->getEvacuation_car_list(),
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'evacuation_type_id')->widget(Select2::className(), [
        'data' => $model->getEvacuation_type_list(),
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
