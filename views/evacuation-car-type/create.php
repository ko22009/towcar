<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EvacuationCarType */

$this->title = 'Create Evacuation Car Type';
$this->params['breadcrumbs'][] = ['label' => 'Evacuation Car Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="evacuation-car-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
