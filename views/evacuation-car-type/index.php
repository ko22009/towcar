<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use app\models\EvacuationCarType;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EvacuationCarTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Evacuation Car Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="evacuation-car-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Evacuation Car Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'evacuation_car_id',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'evacuation_car_id',
                    'data' => EvacuationCarType::getEvacuation_car_list(),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'hideSearch' => true,
                    'options' => [
                        'placeholder' => 'Выберите №',
                    ]
                ]),
                'value' => function($model) {
                    /* @var app\models\EvacuationCarType $model */
                    return $model->getEvacuation_car_name();
                }
            ],
            [
                'attribute' => 'evacuation_type_id',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'evacuation_type_id',
                    'data' => EvacuationCarType::getEvacuation_type_list(),
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'hideSearch' => true,
                    'options' => [
                        'placeholder' => 'Выберите №',
                    ]
                ]),
                'value' => function($model) {
                    /* @var app\models\EvacuationCarType $model */
                    return $model->getEvacuation_type_name();
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
