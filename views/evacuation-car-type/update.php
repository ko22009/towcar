<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EvacuationCarType */

$this->title = 'Update Evacuation Car Type: ';
$this->params['breadcrumbs'][] = ['label' => 'Evacuation Car Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="evacuation-car-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
