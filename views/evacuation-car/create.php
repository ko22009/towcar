<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EvacuationCar */

$this->title = 'Create Evacuation Car';
$this->params['breadcrumbs'][] = ['label' => 'Evacuation Cars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="evacuation-car-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
