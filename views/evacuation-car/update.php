<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EvacuationCar */

$this->title = 'Update Evacuation Car: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Evacuation Cars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="evacuation-car-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
