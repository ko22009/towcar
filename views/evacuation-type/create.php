<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EvacuationType */

$this->title = 'Create Evacuation Type';
$this->params['breadcrumbs'][] = ['label' => 'Evacuation Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="evacuation-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
