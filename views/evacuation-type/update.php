<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EvacuationType */

$this->title = 'Update Evacuation Type: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Evacuation Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="evacuation-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
