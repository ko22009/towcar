<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FirmCustomerCar */

$this->title = 'Create Firm Customer Car';
$this->params['breadcrumbs'][] = ['label' => 'Firm Customer Cars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="firm-customer-car-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
