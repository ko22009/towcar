<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FirmCustomerCar */

$this->title = 'Update Firm Customer Car: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Firm Customer Cars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="firm-customer-car-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
