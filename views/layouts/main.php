<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

array_unshift($this->assetBundles, AppAsset::register($this));
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Yii::$app->name ?><?= isset($this->title) ? ' | ' . $this->title : ''; ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            [
                'label' => 'Заявки',
                'items' => [
                    ['label' => 'Заявки', 'url' => '/application'],
                    ['label' => 'История изменения заявок', 'url' => '/application-history'],
                    ['label' => 'Источник трафика', 'url' => '/source-traffic'],
                    ['label' => 'Что с машиной', 'url' => '/what-is-it'],
                    ['label' => 'Стадии заявок', 'url' => '/stage'],
                    ['label' => 'Типы даты начала заявки', 'url' => '/date-start-type'],
                    ['label' => 'Типы документов', 'url' => '/doc-type'],
                ],
            ],
            [
                'label' => 'Возврат денег',
                'items' => [
                    ['label' => 'Возврат денег', 'url' => '/cash-back'],
                    ['label' => 'Заявки, которые относятся к периодам', 'url' => '/cash-back-application'],
                    ['label' => 'История выплат', 'url' => '/cash-back-payment'],
                    ['label' => 'Типы оплат', 'url' => '/payment-type'],
                ],
            ],
            [
                'label' => 'Затраты',
                'items' => [
                    ['label' => 'Затраты', 'url' => '/cost'],
                    ['label' => 'Документы к затратам', 'url' => '/cost-doc'],
                    ['label' => 'Типы затрат', 'url' => '/cost-type'],
                    ['label' => 'Субъекты типа затрат', 'url' => '/subject-cost-type'],
                ],
            ],
            [
                'label' => 'Клиент',
                'items' => [
                    ['label' => 'Клиент', 'url' => '/customer'],
                    ['label' => 'Марка машины клиента', 'url' => '/firm-customer-car'],
                    ['label' => 'Модель машины клиента', 'url' => '/model-customer-car'],
                ],
            ],
            [
                'label' => 'Автопарк',
                'items' => [
                    ['label' => 'Автопарк', 'url' => '/evacuation-car'],
                    ['label' => 'Типы эвакуаторов', 'url' => '/evacuation-type'],
                    ['label' => 'Эвакуаторы - типы эвакуаторов', 'url' => '/evacuation-car-type'],
                ],
            ],
            [
                'label' => 'Работники',
                'items' => [
                    ['label' => 'Работники', 'url' => '/worker'],
                    ['label' => 'Должности', 'url' => '/position'],
                ],
            ],
            ['label' => 'Диспетчерские', 'url' => ['/dispatcher']],
            /*['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],*/
            Yii::$app->user->isGuest ? (
            ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
