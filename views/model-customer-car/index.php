<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\ModelCustomerCar;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ModelCustomerCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Model Customer Cars';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model-customer-car-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Model Customer Car', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'firm_id',
                'filter' => ModelCustomerCar::getFirm_customer_list(),
                'value' => function($model) {
                    /* @var app\models\ModelCustomerCar $model */
                    return $model->getFirm_name();
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
