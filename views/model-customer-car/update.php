<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ModelCustomerCar */

$this->title = 'Update Model Customer Car:';
$this->params['breadcrumbs'][] = ['label' => 'Model Customer Cars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="model-customer-car-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
