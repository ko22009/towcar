<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SourceTraffic */

$this->title = 'Create Source Traffic';
$this->params['breadcrumbs'][] = ['label' => 'Source Traffics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="source-traffic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
