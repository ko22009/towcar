<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $data yii\data\ActiveDataProvider */

$this->title = 'Стадии заявок';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stage-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $data,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
