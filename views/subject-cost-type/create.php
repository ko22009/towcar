<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubjectCostType */

$this->title = 'Create Subject Cost Type';
$this->params['breadcrumbs'][] = ['label' => 'Subject Cost Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subject-cost-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
