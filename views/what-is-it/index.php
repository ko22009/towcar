<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WhatIsItSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'What Is Its';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="what-is-it-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $data,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name:ntext',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
