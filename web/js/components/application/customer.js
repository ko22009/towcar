import {Component} from '~/helpers/Component.js';

export default Component.create('application-customer_id', class Component {
	constructor($block) {
		Component.self = this;
		Component.self.block = $block[0];
		Component.self.$block = $block;
		Component.self.options = {
			matcher: Component.self.matchCustom,
			allowClear: true,
			theme: "krajee",
			placeholder: "",
			language: "ru"
		};
		if(Component.self.$block.attr('name') == 'Application[customer_id]')
		{
			Component.self.options.width = '100%';
		}
		Component.self.force = false;
		Component.self.order = 0;
		// какое-то условие и загрузка
		Component.self.init();
	}

	init() {
		Component.self.$block.select2(Component.self.options);
		$(document).on('keyup', '.select2-search__field', Component.self.keyup);
	}

	keyup() {

		var part = this.value.split('|');

		var left = '';
		if (part[0]) left = part[0].trim();
		var right = '';
		if (part[1]) right = part[1].trim();

		Component.self.order = 0;
		Component.self.force = false;

		if ($.isNumeric(left[0]) || left[0] == '+') {
			if (/([^0-9\.&\+&\(&\)]+)/g.test(left)) Component.self.force = true;
			Component.self.order = 1;
			left = left.replace(/([^0-9\.&\+&\(&\)]+)/g, '');

		} else if ($.isNumeric(right[0]) || right[0] == '+') {
			if (/([^0-9\.&\+&\(&\)]+)/g.test(right)) Component.self.force = true;
			Component.self.order = 2;
			right = right.replace(/([^0-9\.&\+&\(&\)]+)/g, '');
		}
		if (Component.self.order != 0) {
			if (part.length == 2) this.value = left + ' | ' + right;
			else this.value = left;
			Component.self.order = 0;
		}
		if (Component.self.force) {
			$(this).sendkeys(" ");
			this.value = this.value.slice(0, -1);
		}
	}

	matchCustom(params, data) {
		// If there are no search terms, return all of the data
		if ($.trim(params.term) === '') {
			return data;
		}

		// Do not display the item if there is no 'text' property
		if (typeof data.text === 'undefined') {
			return null;
		}

		var part = data.text.toLowerCase().split('|');
		var left = '';
		if (part[0]) left = part[0].trim();
		var right = '';
		if (part[1]) right = part[1].trim();
		var part_params = params.term.toLowerCase().split('|');

		var left_params = '';
		if (part_params[0]) left_params = part_params[0].trim();
		var right_params = '';
		if (part_params[1]) right_params = part_params[1].trim();

		right = right.replace(/^\+|\(|\)|\s+/g, '');
		if (left_params[0] == '+') left_params = left_params.replace(/^\+|\(|\)|_/g, '');
		else if (right_params[0] == '+') right_params = right_params.replace(/^\+|\(|\)|_/g, '');

		// `params.term` should be the term that is used for searching
		// `data.text` is the text that is displayed for the data object
		// две части обязательны, порядок не важен. Второе условие про отдельные элементы
		if (left_params && right_params && (left.indexOf(left_params) > -1 && right.indexOf(right_params) > -1 || left.indexOf(right_params) > -1 && right.indexOf(left_params) > -1)
			|| (left.indexOf(left_params) > -1 && right_params == '' || right.indexOf(left_params) > -1 && right_params == '')
		) {
			var modifiedData = $.extend({}, data, true);
			return modifiedData;
		}

		// Return `null` if the term should not be displayed
		return null;
	}

}, Component.ID_TYPE);

