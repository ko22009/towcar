import {Component} from '~/helpers/Component.js';

export default Component.create('applicationhistory-evacuation_type_id', class Component {

	constructor($block) {
		Component.self = this;
		Component.self.block = $block[0];
		Component.self.$block = $block;
		// какое-то условие и загрузка
		Component.self.init();
	}

	init() {
		Component.self.$block.change(Component.self.change);
	}

	change() {
		var id = $(this).val();
		$.ajax({
			url: "/application-history/evacuation-type-change",
			data: {id: id},
			success: Component.self.refresh_evacuation_car
		});
	}

	refresh_evacuation_car(data) {
		var $el = $("#applicationhistory-evacuation_car_id");
		$el.empty();
		data = $.parseJSON(data);
		$.each(data, function (key, value) {
			$el.append($("<option></option>").attr("value", key).text(value));
		});
	}

}, Component.ID_TYPE);
