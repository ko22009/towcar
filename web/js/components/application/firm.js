import {Component} from '~/helpers/Component.js';

export default Component.create('application-firm_id', class Component {
	constructor($block) {
		Component.self = this;
		Component.self.block = $block[0];
		Component.self.$block = $block;
		Component.self.init();
	}

	init() {
		Component.self.$block.change(Component.self.change);
	}

	change() {
		var id = $(this).val();
		$.ajax({
			url: "/application/firm-change",
			data: {id: id},
			success: Component.self.refresh_model_customer_car
		});
	}

	refresh_model_customer_car(data) {
		var $el = $("#application-model_customer_car_id");
		$el.empty();
		data = $.parseJSON(data);
		$.each(data, function (key, value) {
			$el.append($("<option></option>").attr("value", key).text(value));
		});
	}

}, Component.ID_TYPE);
