import {Component} from '~/helpers/Component.js';

export default Component.create('application_history-stage_id', class Component {

	constructor($block) {
		Component.self = this;
		Component.self.block = $block[0];
		Component.self.$block = $block;
		Component.self.options = {
			allowClear: true,
			theme: "krajee",
			placeholder: "",
			language: "ru",
			width: '100%'
		};
		// какое-то условие и загрузка
		Component.self.init();
	}

	init() {
		var selected = Component.self.$block.val();
		if(!selected) {
			Component.self.$block.val(1);
		}
		Component.self.$block.select2(Component.self.options);
	}

}, Component.ID_TYPE);

