// перезагрузка данных эвакуаторов при изменении типа эвакуции
import './evacuation_type';
import './firm';
import './customer';
import './state_number_country';
import './state_number_reg';
import './history_stage';