import {Component} from '~/helpers/Component.js';

export default Component.create('application-state_number_country_id', class Component {
	constructor($block) {
		Component.self = this;
		Component.self.block = $block[0];
		Component.self.$block = $block;
		Component.self.options = {
			allowClear: true,
			theme: "krajee",
			placeholder: "",
			language: "ru"
		};
		if(Component.self.$block.attr('name') == 'Application[state_number_country_id]')
		{
			Component.self.options.width = '100%';
		}
		// какое-то условие и загрузка
		Component.self.init();
	}

	init() {
		Component.self.$block.select2(Component.self.options);
		Component.self.default_select_country();
		Component.self.$block.change(Component.self.change);
		$(Component.prototype.type + Component.prototype._componentName).change(Component.self.country_change);
	}

	default_select_country(){
		$(Component.prototype.type + Component.prototype._componentName + " option").each(function(){
			if($(this).text() == "Россия"){
				$(this).attr("selected","selected");
				Component.self.change();
				$(Component.prototype.type + Component.prototype._componentName).change();
				return false;
			}
		});
	}

	country_change() {
		$("#application-state_number_reg option").each(function(){
			if($(this).text().indexOf("74") >= 0){
				$(this).attr("selected","selected");
				$(Component.prototype.type + Component.prototype._componentName).change();
				return false;
			}
		});
	}

	change() {
		$.ajax({
			url: "/region/region-change",
			data: { country: $('#application-state_number_country_id').val() },
			success: function (data) {
				var $el = $("#application-state_number_reg");
				$el.empty();
				data = $.parseJSON(data);
				$.each(data, function (key, value) {
					$el.append($("<option></option>").attr("value", key).text(value));
				});
			}
		});
	}

}, Component.ID_TYPE);

