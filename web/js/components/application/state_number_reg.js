import {Component} from '~/helpers/Component.js';

export default Component.create('application-state_number_reg', class Component {
	constructor($block) {
		Component.self = this;
		Component.self.block = $block[0];
		Component.self.$block = $block;
		Component.self.options = {
			allowClear: true,
			theme: "krajee",
			placeholder: "",
			language: "ru"
		};
		if(Component.self.$block.attr('name') == 'Application[state_number_reg]')
		{
			Component.self.options.width = '100%';
		}
		Component.self.force = false;
		Component.self.order = 0;
		// какое-то условие и загрузка
		Component.self.init();
	}

	init() {
		Component.self.$block.select2(Component.self.options);
	}

}, Component.ID_TYPE);

