import {Component} from '~/helpers/Component.js';

export default Component.create('country_modal', class Component {
	constructor($block) {
		Component.self = this;
		Component.self.block = $block[0];
		Component.self.$block = $block;
		Component.self.$form = $(Component.prototype.type + Component.prototype._componentName + ' form');
		Component.self.$button = $(Component.prototype.type + Component.prototype._componentName + ' button[type=submit]');
		Component.self.init();
	}

	init() {
		Component.self.$form.submit(function (e) {
			e.preventDefault();
		});
		Component.self.$button.click(Component.self.click);
	}

	click() {
		$.ajax({
			type: "POST",
			url: Component.self.$form.attr('action'),
			data: Component.self.$form.serialize(),
			success: function (data) {
				Component.self.$block.modal('toggle');
				if (data == 1) {
					$.notify({
						message: 'Страна успешно создана'
					}, {
						// settings
						type: 'success'
					});

					Component.self.$form[0].reset();

					$.ajax({
						url: "/country/country-change",
						success: function (data) {
							var $el = $("#application-state_number_country_id");
							$el.empty();
							data = $.parseJSON(data);
							$.each(data, function (key, value) {
								$el.append($("<option></option>").attr("value", key).text(value));
							});
						}
					});

				} else {
					$.notify({
						message: 'Ошибка в создании страны'
					}, {
						// settings
						type: 'danger'
					});
				}
			}
		});
	}

}, Component.ID_TYPE);
