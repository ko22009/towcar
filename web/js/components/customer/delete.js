import {Component} from '~/helpers/Component.js';

export default Component.create('customer-delete', class Component {
	constructor($block) {
		Component.self = this;
		Component.self.block = $block[0];
		Component.self.$block = $block;
		Component.self.init();
	}

	init() {
		Component.self.$block.click(Component.self.remove);
	}

	remove(e) {
		e.preventDefault();
		var id = $(this).data('id');
		$.ajax({
			type: "post",
			url: "/customer/delete",
			data: {id: id, partial: 1},
			success: function (data) {
				$('.customer-index').html(data);
			}
		});
	}

}, Component.CLASS_TYPE);

