import {Component} from '~/helpers/Component.js';

export default Component.create('region-country_id', class Component {
	constructor($block) {
		Component.self = this;
		Component.self.block = $block[0];
		Component.self.$block = $block;
		Component.self.options = {
			matcher: Component.self.matchCustom,
			allowClear: true,
			theme: "krajee",
			placeholder: "",
			language: "ru"
		};
		if(Component.self.$block.attr('name') == 'Region[country_id]')
		{
			Component.self.options.width = '100%';
		}
		Component.self.init();
	}

	init() {
		Component.self.$block.select2(Component.self.options);
	}

}, Component.ID_TYPE);


