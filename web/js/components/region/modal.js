import {Component} from '~/helpers/Component.js';

export default Component.create('region_modal', class Component {
	constructor($block) {
		Component.self = this;
		Component.self.block = $block[0];
		Component.self.$block = $block;
		Component.self.$form = $(Component.prototype.type + Component.prototype._componentName + ' form');
		Component.self.$button = $(Component.prototype.type + Component.prototype._componentName + ' button[type=submit]');
		Component.self.$link = $("a[data-target='" + Component.prototype.type + Component.prototype._componentName + "']");
		Component.self.init();
	}

	init() {
		Component.self.$form.submit(function (e) {
			e.preventDefault();
		});
		Component.self.$button.click(Component.self.click);
		Component.self.$link.click(Component.self.click_link);
	}
	click_link() {
		var country = $('#application-state_number_country_id').val();
		var select = Component.prototype.type + Component.prototype._componentName + ' select';
		if(country) {
			$("#region-country_id option[value='" + country +"']").attr("selected", "selected");
			$(select).change();
		} else {
			$("#region-country_id option").removeAttr("selected");
			$(select).change();
			Component.self.$form[0].reset();
		}
	}
	click() {
		$.ajax({
			type: "POST",
			url: Component.self.$form.attr('action'),
			data: Component.self.$form.serialize(),
			success: function (data) {
				Component.self.$block.modal('toggle');
				if (data == 1) {
					$.notify({
						message: 'Регион успешно создан'
					}, {
						// settings
						type: 'success'
					});

					Component.self.$form[0].reset();

					$.ajax({
						url: "/region/region-change",
						data: { country: $('#application-state_number_country_id').val() },
						success: function (data) {
							var $el = $("#application-state_number_reg");
							$el.empty();
							data = $.parseJSON(data);
							$.each(data, function (key, value) {
								$el.append($("<option></option>").attr("value", key).text(value));
							});
						}
					});

				} else {
					$.notify({
						message: 'Ошибка в создании региона'
					}, {
						// settings
						type: 'danger'
					});
				}
			}
		});
	}

}, Component.ID_TYPE);

