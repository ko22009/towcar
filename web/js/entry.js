import {Component, $} from '~/helpers/Component';

window.$ = $;
window.jQuery = $;

/**
 * libs
 */

require('select2')($);
require('jquery-sendkeys');
require('jquery.maskedinput');

// bower
require('bootstrap');
require('bootstrap-notify');

/**
 * modules
 */

/**
 * components
 */
import './components/application';
import './components/customer';
import './components/country';
import './components/region';

$(() => {

	// если что-то на странице меняется, перезагружаем наши компоненты
	Component.domObserver($('body')[0], function() {
		Component.initAll();
	});

	Component.initAll();
});

