import _ from 'lodash';
import $ from 'jquery';

const map = {};
const cache = {};

const CLASS_TYPE = '.';
const ID_TYPE = '#';

/**
 * Заносит все компоненты Component.create('map', class {... в переменную map, индекс является первый параметр
 * значение, второй параметр. Сначала идет проверка на сущ.компонента, если есть, про пропускаем.
 * @param componentName
 * @param ComponentClass
 */
function writeInMap(componentName, ComponentClass) {
	if (componentName in map) {
		return;
	}
	map[componentName] = ComponentClass;
}

const Component = class Component {

	/**
	 * @return {string}
	 */
	static get CLASS_TYPE() {
		return CLASS_TYPE;
	}

	/**
	 * @return {string}
	 */
	static get ID_TYPE() {
		return ID_TYPE;
	}

	static domObserver(targetNode, callback){
// Options for the observer (which mutations to observe)
		var config = { childList: true, subtree: true };

// Create an observer instance linked to the callback function
		var observer = new MutationObserver(callback);

// Start observing the target node for configured mutations
		observer.observe(targetNode, config);
	}

	static initAll() {
		for (const component in map) {
			if (!map.hasOwnProperty(component)) {
				continue;
			}
			$('body').find(`${map[component].prototype.type + component}`).each((ind, block) => {
				Component.get($(block), map[component]);
			});
		}
	}

	static get($block, Component) {
		$block = $block.eq(0);

		const name = Component.prototype._componentName;
		if (Component.prototype.type == this.CLASS_TYPE && !$block.hasClass(name) || Component.prototype.type == this.ID_TYPE && !$block.is('#' + name)) {
			const msg = `Данный элемент не является компонентом ${name}`;
			console.error(msg);
			throw new Error(msg);
		}

		function createInstance() {
			const component = new Component($block);
			let id = $block.data('componentID');

			if (!component.$block) {
				component.$block = $block;
			}

			if (!cache[name]) {
				cache[name] = {};
			}

			if (!id) {
				id = _.uniqueId();
				$block.data('componentID', id);
			}

			cache[name][id] = component;
			return component;
		}

		function getFromCache() {
			const id = $block.data('componentID');
			let componentFromCache;

			if (id) {
				componentFromCache = cache[name][id];

				if (componentFromCache) {
					return componentFromCache;
				}
			}

			return null;
		}

		if ($block.hasClass(`${name}_js_inited`)) {
			const componentFromCache = getFromCache();

			if (componentFromCache) {
				return componentFromCache;
			} else {
				return createInstance();
			}
		}

		$block.addClass(`${name}_js_inited`);
		return createInstance();
	}

	static create(componentName, Component, TYPE = this.CLASS_TYPE) {
		const proto = Object.create(Component.prototype);

		Object.getOwnPropertyNames(Component.prototype).forEach(prop => {
			if (proto.hasOwnProperty(prop)) {
				return;
			}

			Object.defineProperty(
				proto,
				prop,
				Object.getOwnPropertyDescriptor(Component.prototype, prop));
		});


		Component.prototype = proto;

		Object.defineProperty(Component.prototype, '_componentName', {
			value: componentName
		});

		Object.defineProperty(Component.prototype, 'type', {
			value: TYPE
		});

		writeInMap(componentName, Component);

		return Component;
	}
};

export {Component, $};
