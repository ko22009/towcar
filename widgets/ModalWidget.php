<?php

namespace app\widgets;

use yii\base\Widget;

class ModalWidget extends Widget {
    public $model;
    public $view;
    public $id;
    public $title;
    public $button;

    public function init() {
        parent::init();
    }

    public function run() {
        $view = $this->render($this->view, [
            'model' => new $this->model,
            'modal' => 1
        ]);
        return $this->render('modal', ['id' => $this->id, 'title' => $this->title, 'view' => $view, 'button' => $this->button]);
    }
}