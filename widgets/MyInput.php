<?php

namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class MyInput extends Widget {
    public $model;
    public $attribute;
    public $type = 'text';
    public $view;
    public $modal_id;
    public $list;
    public $col_size = 'col-md-3';

    public function init() {
        parent::init();
    }

    public function run() {
        $_id = $this->attribute;
        $table_name = $this->model->tableSchema->name;
        $id = $table_name . '-' . $_id;

        $full_class_name = get_class($this->model);
        $count = strlen($full_class_name);
        $num_elem = strpos(strrev($full_class_name), "\\");
        $class_name = substr($full_class_name, $count - $num_elem, $count);

        $input = Html::tag('label', $this->model->getAttributeLabel($this->attribute), ['class' => 'control-label', 'for' => $id]);

        if($this->type == 'select2')
        {
            $input .= MySelect2::widget(['view' => $this->view, 'model' => $this->model, 'modal_id' => $this->modal_id, 'list' => $this->list, 'attribute' => $this->attribute]);
        } else $input .= Html::tag('input', '', ['class' => 'form-control', 'id' => $id, 'name' => $class_name . '[' . $this->attribute . ']', 'value' => $this->model[$this->attribute], 'type'=> $this->type]);

        $input .= Html::tag('div', '', ['class' => 'help-block']);

        $field_class = 'field-' . $table_name . '-' . $this->attribute;

        $input = Html::tag('div', $input, ['class' => 'form-group clearfix ' . $this->col_size . ' ' . $field_class]);

        return $input;
    }
}