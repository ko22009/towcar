<?php

namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class MySelect2 extends Widget {
    public $model;
    public $attribute;
    public $list;
    public $modal_id = false;

    public function init() {
        parent::init();
    }

    public function run() {
        $_id = $this->attribute;
        $table_name = $this->model->tableSchema->name;
        $id = $table_name . '-' . $_id;

        $full_class_name = get_class($this->model);
        $count = strlen($full_class_name);
        $num_elem = strpos(strrev($full_class_name), "\\");
        $class_name = substr($full_class_name, $count - $num_elem, $count);

        $list[] = Html::tag('option');
        $options = [];
        foreach ($this->list as $key => $value) {
            if ($this->model[$_id] == $key ) {
                $options['selected'] = 'selected';
            } else unset($options['selected']);
            $options['value'] = $key;
            $options['class'] = 'username';
            $list[] = Html::tag('option', Html::encode($value), $options);
        }

        $select = Html::tag('select', implode(" ", $list), ['id' => $id, 'name' => $class_name . '[' . $_id . ']']);

        if($this->modal_id)
        {
            $plus = Html::tag('span', '', ["class" => "glyphicon glyphicon-plus"]);
            $content = Html::tag('a', $plus, ["class" => "btn btn-primary", "data-target" => $this->modal_id, "data-toggle" => "modal"]);
            $button = Html::tag('div', $content, ['class' => 'input-group-btn']);
            $select = $select . $button;
        }

        $end = Html::tag('div', $select, ['class' => 'input-group input-group-md select2-bootstrap-append']);

        return $end;
    }
}