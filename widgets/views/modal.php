<?php if ($button) { ?>
    <a class="btn btn-primary" data-toggle="modal" data-target="#<?= $id ?>">
        <span class="glyphicon glyphicon-plus"></span>
    </a>
<?php } else { ?>
    <!-- Modal -->
    <div class="modal fade" id="<?= $id ?>" tabindex="-1" role="dialog" aria-labelledby="#<?= $id ?>Label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="<?= $id ?>Label"><?= $title ?></h4>
                </div>
                <div class="modal-body">
                    <?= $view ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>